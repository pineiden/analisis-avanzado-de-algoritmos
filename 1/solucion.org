# -*- coding: utf-8 -*-
#+TITLE: Tarea 1: Cola con posicionamiento aleatorio
#+AUTHOR: David Pineda Osorio
#+OPTIONS: toc:nil
#+LANGUAGE: es
#+SETUPFILE: ../setup_latex_two_col.org
#+OPTIONS: broken-links:t
#+OPTIONS: ^:nil

* Problema

Se considera una cola que cada vez que un elemento llega a la primera
posición se toma este y se vuelve a colocar aleatoriamente sobre la
misma cola.

#+caption: Elemento llegando al inicio y relocalizando
#+name: chart:1
#+attr_html: :width 500px
#+attr_latex: :width 200px
[[file:./cola_0.png]]

Se calcula el /número total esperado de pasos/ hasta que el último
elemento del estado inicial (la 'o' en la imagen) llegue al primer lugar.

En concreto, pueden suceder dos casos:

- el elemento a reubicar se pone /antes/ de 'O'
- el elemento a reubicar se pone /despúes/ de 'O'

Al llegar 'O' a la primera posición se termina el proceso.

** Análisis matemático.

Se estudia este fenómeno a partir de lo que ocurre con 'O'. Y lo que
ocurre con 'O' dependerá de lo que pase con el primer valor en cola en
ese momento.

Considerando que es un fenómeno *aleatorio* y tiene solo dos casos
posibles. Se puede asignar como:

- avanzar p :: la probabilidad de avanzar
- quedarse q :: la probabilidad quedarse en el mismo lugar

En que tienen una relación que cumple

$$
q = 1 - p
$$

Considerando que estos valores dependen de la posición de 'O' en la
cola. Es decir, según esta tabla.

| posición '0' | p       | q         |
|--------------+---------+-----------|
| n            | 1/n     | 1-1/n     |
| n-1          | 2/n     | 1-2/n     |
| ...          |         |           |
| j            | (j-1)/n | (n-j+1)/n |
| ...          |         |           |
| 2            | 1-2/n   | 2/n       |
| 1            | 1-1/n   | 1/n       |

Visualmente, los dos casos se pueden representar de manera general
por la figura [[chart:2]].

#+caption: Secuencia en posición 'j'
#+name: chart:2
#+attr_html: :width 500px
#+attr_latex: :width 200px
[[file:./secuencia.png]]

Representado un grafo de estados.

#+begin_src dot :file queue.png
digraph closed_queue {
rankdir=LR;
"FIN" [shape=box]
n -> "n-1" [label="p"]
n -> n [label="q"]
"n-1" -> "..." [label="p"]
"n-1" -> "n-1" [label="q"]
"..." -> "2" [label="p"]
"..." -> "..." [label="q"]
"2" -> "2" [label="q"]
"2" -> "1" [label="p"]
"1" -> "1" [label="q"]
"1" -> "FIN"
}
#+end_src

#+RESULTS:
[[file:queue.png]]

Entonces, la probabilidad de avanzar al siguiente estado, teniendo a 'O' en /j/ será.

$$
P(X_j) = p = \frac {n-j+1} {n}
$$

Con esto es posible calcular la probabilidad de /k/ instantes de cambiar de estado (p),
considerando que se repite la posición anteriormente /k-1/ veces. 

$$
P(k) = q^{k-1} p
$$

En conjunto, se puede estimar el número esperado de intentos para pasar
al siguiente estado, tomando todos los posibles /k/.

\begin{multline}
E[X_j] &= \sum_{k>=1} k P(k)  \\
&= \sum_{k>=1} k q^{k-1} p \\
&= \frac {1} {p} \\
\end{multline}

Lo que nos lleva a la cantidad, dada la posición /j/ de 'O'.

$$
E[X_j]  = \frac {n} {n - j + 1} 
$$

Encontramos el número esperado de intentos para
avanzar desde la posición /n/ hasta /1/.

$$
N = n * (1 + \frac {1} {2} + \frac {1} {3} ... \frac {1} {n-1} )
$$

Notando que esta cantidad dependerá fuertemente de la /cantidad/ de
elementos que tenga la cola. Lo que nos descubre la definición del
/armónico/.

$$
H_n = \sum_{1<=k<=n} \frac {1} {k}
$$

** Relación de los números armónicos

*** Relación secuencial de gamma

Se tiene la funcioń /Gamma/ definida por la siguiente integral.

$$
\Gamma(z) = \int_{0}^{\infty} e^{-t} t^{z-1} dt 
$$

Se necesita demostrar que el siguiente valor entero (una secuencia)
se corresponde con.

$$
\Gamma(z+1) = z \Gamma(z)
$$

Es sencillo, ya que realizando ingración por partes, se tendrá.

En que las partes sean $u=t^{z}$ y $v=e^{-t}$, resulta en.

$$
\Gamma(z+1) =  \int_{0}^{\infty} e^{-t} t^{z} dt  
$$

Finalmente se tendrá la relación buscada.


\begin{multiline}
\Gamma(z+1) &=  e^{-t} t^{z} \Bigg|_{0}^{\infty} - \int_{0}^{\infty} z e^{-t} t^{z-1} dt = z \Gamma(z)
\end{multiline}
 

*** Encontrando Psi relacionada con Gamma.

Por definición 

$$
\Psi (z) = \frac {\Gamma'(z)} {\Gamma(z)}
$$

Entonces, la secuencia para /z+1/ se encontrará en.

$$
\Psi (z+1) = \frac {\Gamma'(z+1)} {\Gamma(z+1)}
$$

Se deriva el numerado.

$$
\Gamma' (z+1) = \frac {d (z \Gamma(z))} {dz} = \Gamma(z) + z \Gamma'(z)
$$

En consecuencia.

$$
\Psi (z+1) = \frac {\Gamma(z) + z \Gamma'(z)} {z \Gamma(z)} 
$$

Finalmente se tendrá.

$$
\Psi (z+1)= \frac {1} {z} + \Psi(z)
$$

*** Relación Psi y armónico.

Sabiendo que,  por definición que el armónico para 'n' será.

$$
H_n = \sum_{1<=k<=n} \frac {1} {k}
$$

Y las secuencia de $\Psi$ se puede expresar también por.

$$
\Psi(n+1) = \sum_{1<=k<=n} \frac {1} {k} + \Psi(1)
$$

La diferencia buscada será entonces.

$$
H_n - \Psi_{n+1} = - \Psi_{1}
$$
