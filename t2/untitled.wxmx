PK     ùº$UñBH         mimetypetext/x-wxmathmlPK     ùº$UQdBV5  5  
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/wxMaxima-developers/wxmaxima.
It also is part of the windows installer for maxima
(https://wxmaxima-developers.github.io/wxmaxima/).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using an text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     ùº$UÝ³       content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created using wxMaxima 19.07.0   -->
<!--https://wxMaxima-developers.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="100" activecell="17">

<cell type="code">
<input>
<editor type="input">
<line>solve_req(a[n]=1+4*a[n-1]-4*a[n-2],a[0]=1,a[1]=2);</line>
</editor>
</input>
<output>
<mth><lbl>(%o2) </lbl><fn><r><fnm>solve_req</fnm></r><r><p><i><r><v>a</v></r><r><v>n</v></r></i><v>=</v><n>4</n><h>*</h><i><r><v>a</v></r><r><v>n</v><v>â</v><n>1</n></r></i><v>â</v><n>4</n><h>*</h><i><r><v>a</v></r><r><v>n</v><v>â</v><n>2</n></r></i><v>+</v><n>1</n><t>,</t><i><r><v>a</v></r><r><n>0</n></r></i><v>=</v><n>1</n><t>,</t><i><r><v>a</v></r><r><n>1</n></r></i><v>=</v><n>2</n></p></r></fn>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>load(solve_req)$</line>
</editor>
</input>
<output>
<mth><t breakline="true">file_search1: </t><v>solve_req</v><t> not found in </t><t>file_search_maxima,file_search_lisp</t><t>.</t><t breakline="true" type="error"> -- an error. To debug this try: debugmode(true);</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>%;</line>
</editor>
</input>
<output>
<mth><lbl>(%o4) </lbl><fn><r><fnm>solve_req</fnm></r><r><p><i><r><v>a</v></r><r><v>n</v></r></i><v>=</v><n>4</n><h>*</h><i><r><v>a</v></r><r><v>n</v><v>â</v><n>1</n></r></i><v>â</v><n>4</n><h>*</h><i><r><v>a</v></r><r><v>n</v><v>â</v><n>2</n></r></i><v>+</v><n>1</n><t>,</t><i><r><v>a</v></r><r><n>0</n></r></i><v>=</v><n>1</n><t>,</t><i><r><v>a</v></r><r><n>1</n></r></i><v>=</v><n>2</n></p></r></fn>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>%a[0]=1</line>
<line>;</line>
</editor>
</input>
<output>
<mth><lbl>(%o5) </lbl><i><r><v>%a</v></r><r><n>0</n></r></i><v>=</v><n>1</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>a[1]=2;</line>
</editor>
</input>
<output>
<mth><lbl>(%o6) </lbl><i><r><v>a</v></r><r><n>1</n></r></i><v>=</v><n>2</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>a[n]:=Â 1 + 4*a[n-1] - 4* a[n-2];</line>
</editor>
</input>
<output>
<mth><lbl userdefined="yes" userdefinedlabel="a[n]" tooltip="(%o7) ">(%o7) </lbl><i><r><v>a</v></r><r><v>n</v></r></i><t>:=</t><v>Â 1</v><v>+</v><n>4</n><h>*</h><i><r><v>a</v></r><r><v>n</v><v>â</v><n>1</n></r></i><v>+</v><r><p><v>â</v><n>4</n></p></r><h>*</h><i><r><v>a</v></r><r><v>n</v><v>â</v><n>2</n></r></i>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>a[2];</line>
</editor>
</input>
<output>
<mth><t breakline="true">Message from the stdout of Maxima: </t><t breakline="true">Unrecoverable error: bind stack overflow.</t><t breakline="true">Message from maxima&apos;s stderr stream: </t><t breakline="true" type="error">Maxima exited...</t><t breakline="true" type="error">Trying to restart Maxima.</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>a[0]=1</line>
<line>;</line>
</editor>
</input>
<output>
<mth><lbl>(%o1) </lbl><i><r><v>a</v></r><r><n>0</n></r></i><v>=</v><n>1</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>a[3];</line>
</editor>
</input>
<output>
<mth><lbl>(%o2) </lbl><i><r><v>a</v></r><r><n>3</n></r></i>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>a[3] ;</line>
</editor>
</input>
<output>
<mth><lbl>(%o3) </lbl><i><r><v>a</v></r><r><n>3</n></r></i>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>load</line>
<line>;</line>
</editor>
</input>
<output>
<mth><lbl>(%o6) </lbl><v>load</v>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>load(solve_req);</line>
</editor>
</input>
<output>
<mth><t breakline="true">file_search1: </t><v>solve_req</v><t> not found in </t><t>file_search_maxima,file_search_lisp</t><t>.</t><t breakline="true" type="error"> -- an error. To debug this try: debugmode(true);</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>solve_rec(x[n]-2*x[n-1]=n, x[n], x[1]=2);</line>
</editor>
</input>
<output>
<mth><lbl>(%o16) </lbl><i><r><v>x</v></r><r><v>n</v></r></i><v>=</v><n>5</n><h>*</h><e><r><n>2</n></r><r><v>n</v><v>â</v><n>1</n></r></e><v>â</v><v>n</v><v>â</v><n>2</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>solve_rec(a[n]=1+4*a[n-1]-4*a[n-2],a[0]=1,a[n],a[1]=2);</line>
</editor>
</input>
<output>
<mth><lbl>(%o17) </lbl><t>false</t>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>solve_rec(a[n]=1+4*a[n-1]-4*a[n-2],a[n],a[0]=1,a[1]=2);</line>
</editor>
</input>
<output>
<mth><lbl>(%o18) </lbl><i><r><v>a</v></r><r><v>n</v></r></i><v>=</v><v>n</v><h>*</h><e><r><n>2</n></r><r><v>n</v><v>â</v><n>1</n></r></e><v>+</v><n>1</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>partfrac(1/((x-1)*(x+2)),x);</line>
</editor>
</input>
<output>
<mth><lbl>(%o23) </lbl><f><r><n>1</n></r><r><n>3</n><h>*</h><r><p><v>x</v><v>â</v><n>1</n></p></r></r></f><v>â</v><f><r><n>1</n></r><r><n>3</n><h>*</h><r><p><v>x</v><v>+</v><n>2</n></p></r></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>partfrac(z^2 / ((1-z)*(1-2*z)^2),z);</line>
</editor>
</input>
<output>
<mth><lbl>(%o24) </lbl><f><r><n>3</n></r><r><n>2</n><h>*</h><r><p><n>2</n><h>*</h><v>z</v><v>â</v><n>1</n></p></r></r></f><v>+</v><f><r><n>1</n></r><r><n>2</n><h>*</h><e><r><r><p><n>2</n><h>*</h><v>z</v><v>â</v><n>1</n></p></r></r><r><n>2</n></r></e></r></f><v>â</v><f><r><n>1</n></r><r><v>z</v><v>â</v><n>1</n></r></f>
</mth></output>
</cell>

</wxMaximaDocument>PK      ùº$UñBH                       mimetypePK      ùº$UQdBV5  5  
             5   format.txtPK      ùº$UÝ³                   content.xmlPK      §   F    