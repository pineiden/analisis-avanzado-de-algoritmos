from sympy import series
from pprint import pprint
from sympy import init_printing, symbols, Function

x, h = symbols("x,h")
f = Function("f")
pprint(series(f(x), x, x0=h, n=3))
