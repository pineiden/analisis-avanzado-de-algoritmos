from sympy import symbols, eye, Matrix, diff, Product, oo, simplify
from rich import print
from sympy import init_session
from sympy import init_printing
init_printing()
# oo es infinito


n, z, m = symbols("n z m")
H = Matrix([[-1, 6*z], [0.5, -2]])
I = eye(H.shape[0])
fz = I + (1/n) * H
a0 = Matrix([2, 0])
determinante = fz.det()

print(determinante)
derivation = fz.diff(z)
print(derivation)

Pr = simplify(Product(fz, (n, 1, m)).doit())
print(Pr)

# # la función generatriz de manera vectorial
a_z = Pr * a0

print("AZ", a_z[0].doit())
# # parámetros del polinomio para la fpg
# c_z = Matrix([1, 2]).T
# Pz = (1/m) * c_z * a_z


# print("Probabilidad en z")
# print(Pz)

# values = {z: 1, m: 100}
# result = Pz.subs(values)
# print(result)
