# -*- coding: utf-8 -*-
#+TITLE: Tarea 5: Análisis de árboles de búsqueda ternaria y más.
#+AUTHOR: David Pineda Osorio
#+OPTIONS: toc:nil
#+LANGUAGE: es
#+SETUPFILE: ../setup_latex_one_col.org
#+OPTIONS: broken-links:t
#+OPTIONS: ^:nil

* Introducción 

Se define el *árbol de búsqueda ternaria* (ABT) como aquel en que cada
nodo almacena una /tupla/ de llaves. En este caso son dos llaves
*(a,b)* y cada /subárbol/ se genera a partir de una comparación como
se describe a continuación:

Si se ingresa un valor *x*, la ubicación será:

- x < a :: a la izquierda
- a <= x <= b :: al centro
- b < x :: a la derecha 

Si *x* cae en una hoja vacía, está se va llenando con el valor hasta
completar la cantidad límite (2), en este punto se generan *tres*
nuevas hojas vacías bajo este nodo.

Esto se puede describir mediante el siguiente diagrama.

#+CAPTION: Diagrama de generación de un ABT
#+ATTR_HTML: :width 400px
#+ATTR_LATEX: :width 400px
#+ATTR_ORG: :width 400px
[[file:img/abt.png]]

Como cada nodo contiene *2 slots, espacios o contenedores*, el
análisis nos lleva a considera esta condición. Además al completar el
nodo genera *3* nuevas hojas vacías.                                                                     

Se estudia con esto la probabilidad de que una inserción caiga en una
hoja en particular *Pr {x en clase_i}*, se tendrán dos posibilidades:

- clase 0 :: x cae en hoja vacía $Pr = \frac {2} {n+1}$
- clase 1 :: x cae en hoja con una llave  $Pr = \frac {1} {n+1}$

Con esto ya se puede encontrar la cantidad de hojas que es posible
encontrar a un nivel *k*, dada una cantidad *n* de inserciones
realizadas previamente, para una clase específica *j*.

Se puede observar también que el nodo completo (con dos llaves) se
puede representar o modelar como una hoja de un árbol binario con dos
nodos.

#+CAPTION: Representación hoja completa ABT
#+ATTR_HTML: :width 400px
#+ATTR_LATEX: :width 400px
#+ATTR_ORG: :width 400px
[[file:./img/representacion.png]]

En general deberá ser: lo que había antes, lo que desaparece  y lo que
aparece en reemplazo, asociadas a las probabilidades correspondientes.

En caso de caer en una hoja *clase 0*. Se considera que la clase 0
aporta 2 slots, la clase 1 aporta 1 slot.

$$
a_{n+1,k}^{0} =  a_{n,k}^{0} - \frac {2} {n+1} \frac {a_{n,k}^{0}} {2} + \frac {1}
{n+1} a_{n,k}^{1}
$$

Así, como también para la *clase 1*, en que aparecen tres nuevas hojas vacías.

$$
a_{n+1,k}^{1} =  a_{n,k}^{1} + \frac {1} {n+1} a_{n,k}^{1} + 3*\frac {2}
{n+1} \frac {a_{n,k-1}^{0}} {2}
$$

Evidentemente eso se podrá expresar la generatriz en /z/ como una operación matricial.

$$
\vec{a_{n+1}} (z) = (I + \frac {1} {n+1} H(z))\vec{(a_{n}} (z) =
\vec{f_n(z)} \vec{a_{n}}
$$

En que *H(z)* se verá como una composición de constantes y valores
operados con *z*, además *I* es la identidad.

$$
H(z) = \begin{bmatrix}-1 & +6z\\+0.5 & -2\end{bmatrix}
$$

Así como la condición inicial estará definida por la cantidad de
comparaciones que necesitará el sistema para posicionar el valor.

$$
\vec(a_{0,0}) = \begin{bmatrix} 2 \\ 0 \end{bmatrix}
$$

El problema de esta solución es que aún no se están considerando las
probabilidades de distribuir la insersión dentro de cada hoja o bien
en las ramas vecinas.

Se tiene que X es una variable aleatoria continua. Lo que corresponde
es que la probabilidad total sea 1.

Lo que nos lleva a comparar, para cada clase:

- x->[] ->  [*,] :: P(insercion) = 1
- x->[*,] ->  [*,x] :: P(insercion) = P(x<*)
- x->[*,] ->  [x,*] :: P(insercion) = 1 - P(x<*)
- x->[a,b] -> [a,b] - [x,]_1 :: P(insercion) = P(x<a) = P_a
- x->[a,b] -> [a,b] - [x,]_2 :: P(insercion) = P(a<x<b) = P_ab
- x->[a,b] -> [a,b] - [x,]_3 :: P(insercion) = P(b<x) = P_b

Además, considerando que [] puede tener una inserción a partir de
cualquiera de los diferentes canales por los que se ha enrutado *x*,
habría que considerar cada caso como {I,II,III}

Caso I

$$
a_{n+1,k}^{0,I} =  a_{n,k}^{0,I} - \frac {2P_a} {n+1} \frac {a_{n,k}^{0,I}} {2} + \frac {P_a}
{n+1} a_{n,k}^{1,I,a}
$$

Caso II

$$
a_{n+1,k}^{0,II} =  a_{n,k}^{0} - \frac {2P_{ab}} {n+1} \frac {a_{n,k}^{0,II}} {2} + \frac {P_{ab}}
{n+1} a_{n,k}^{1,II,a}
$$

Caso III

$$
a_{n+1,k}^{0,III} =  a_{n,k}^{0} - \frac {2P_b} {n+1} \frac {a_{n,k}^{0}} {2} + \frac {P_b}
{n+1} a_{n,k}^{1,III,a}
$$

Lo que parece ser correcto, ya que si se da inicio por alguno de los
canales, sea II, Pab=1, Pa=Pb=0. Para n=1.

Entonces, para el caso que la hoja tenga ya una llave. Debe hacer una
comparación para posicionar la inserción y luego añadir tres nuevas
hojas vacías. Según cada caso se tendrá. 

Caso I, x<a. Se posiciona en el slot de la izquierda.

$$
a_{n+1,k}^{1,I,a} =  a_{n,k}^{1,I,a} - \frac {Pa} {n+1} a_{n,k}^{1,I,a} + \frac {2*P_a}
{n+1} (a_{n,k-1}^{0,I} + a_{n,k-1}^{0,II} + a_{n,k-1}^{0,III})
$$


Caso I, b:x>a. Se posiciona en el slot de la derecha.

$$
a_{n+1,k}^{1,I,b} =  a_{n,k}^{1,I,a} - \frac {1-Pa} {n+1}
a_{n,k}^{1,I,a} + \frac {2*(1- P_a)}
{n+1} (a_{n,k-1}^{0,I} + a_{n,k-1}^{0,II} + a_{n,k-1}^{0,III})
$$


Caso II, x<a

$$
a_{n+1,k}^{1,II,a} =  a_{n,k}^{1,II,a} + P_a (\frac {-11} {n+1} a_{n,k}^{1,I,a} + \frac {2}
{n+1} (a_{n,k-1}^{0,I} + a_{n,k-1}^{0,II} + a_{n,k-1}^{0,III}))
$$


Caso II, b:x>a

$$
a_{n+1,k}^{1,II,b} =  a_{n,k}^{1,II,a} +(1-Pa) (- \frac {1} {n+1} a_{n,k}^{1,II,a} + \frac {2}
{n+1} (a_{n,k-1}^{0,I} + a_{n,k-1}^{0,II} + a_{n,k-1}^{0,III}))
$$

Caso III, x<a

$$
a_{n+1,k}^{1,III,a} =  a_{n,k}^{1,III,a} +P_a( \frac {-1} {n+1} a_{n,k}^{1,II,a} + \frac {2}
{n+1} (a_{n,k-1}^{0,I} + a_{n,k-1}^{0,II} + a_{n,k-1}^{0,III}))
$$


Caso III, b:x>a

$$
a_{n+1,k}^{1,III,b} =  a_{n,k}^{1,III,b} + (1-Pa)(\frac {-1} {n+1} a_{n,k}^{1,III,b} + \frac {2}
{n+1} (a_{n,k-1}^{0,I} + a_{n,k-1}^{0,II} + a_{n,k-1}^{0,III}))
$$



Luego,  la generatriz como una expresión
polinomial o multiplicación matricial.

$$
\vec{a_{n+1}} (z) = \vec{f_n(z)}\vec{f_{n-1}(z)}...\vec{f_0(z)}
\vec{a_{0}} = \prod_{i=0}^{n} \vec{f_{i}(z)}\vec{a_{0}}
$$ 

$$
\frac {\partial\vec{a_{n+1}}} {\partial z} = \prod_{i=0}^{n}
\vec{f_{i}(z)}  \sum_{i=0}^{n}
\frac {\vec{f_{i}'(z)}} {|\vec{f_{i}(z)}|}}  \vec{a_{0}} 
$$

De esta manera, nos posibilita expresar la derivada en *z* del modo
siguiente.

Teniendo que |f| es el determinante de f.

$$
det(f) = |I + \frac {1} {n+1} H(z)|= \frac {n^2-3.0*n-3.0*z+2.0} {n^2} 
$$

Y la derivada de f, será:

$$
\vec{f_{n}'(z)} = \begin{bmatrix}0 & +6/n \\ 0 & 0\end{bmatrix}
$$

Con esto, ya será posible conocer la probabilidad para la búsqueda
infructuosa para un elemento aleatorio.

$$
P_{n}(z) = \vec{c_{n}(z)} \vec{a_{n}(z)}= \frac {1} {n} \begin{bmatrix} 2  & 1 \end{bmatrix} \vec{a_{n}(z)}
$$

Se debe comprobar que, para z=1, debe ser 1.

Y el costo esperado consistirá en derivar según *z* y evaluar en 1.
