import math
from sympy import symbols
from sympy.matrices import Matrix
from sympy import *
from sympy import init_printing
from sympy.plotting import plot
import matplotlib.pyplot as plt

init_printing()


def gen(i, j):
    return 1


def solve(a, b, n=4):
    S = Matrix(n, n, gen)
    a1, af, b1, bf = symbols('a1 af b1 bf')
    S[0, 0] = 1
    for i in range(n):
        for j in range(n):
            if i < n - 1 or j < n-1:
                if i-1 < 0 or j-1 < 0:
                    if i - 1 < 0 and j - 1 >= 0:
                        S[i, j] = a * S[i, j-1]
                    elif j - 1 < 0 and i - 1 >= 0:
                        S[i, j] = b * S[i-1, j]
                else:
                    S[i, j] = a * S[i, j-1] + b * S[i-1, j]
            elif i == n - 1 and j == n-1:
                S[i, j] = a * S[i, j-1] + b * S[i-1, j] + b * a1 + a * b1

                resp = S[i, j]
                a1 = a*resp
                b1 = b*resp

    va = [S[m, n-1] for m in range(n-1)]
    vb = [S[n-1, m] for m in range(n-1)]
    af = a * (a1 + sum(va))
    bf = b * (b1 + sum(vb))
    x, y = symbols("x y")
    #n = 7
    system = []
    if n == 7:
        system = [
            a1 - a*(924*a**6*b**6 + a*b1 + a1*b),
            b1 - b*(924*a**6*b**6 + a*b1 + a1*b)
        ]
    if n == 10:
        system = [
            a1 - a*(48620*a**9*b**9 + a*b1 + a1*b),
            b1 - b*(48620*a**9*b**9 + a*b1 + a1*b)
        ]

    result = nonlinsolve(system, [a1, b1])

    resultados = dict(zip(("a1", "b1"), *result))
    pprint(resultados)
    pprint(S)
    pprint("Valores principales:")
    print("S[n-1,n-1]", S[n-1, n-1])
    print("a1=", a1)
    print("b1=", b1)
    print("af=", af)
    print("bf=", bf)
    af = af.replace(a1, resultados["a1"])
    af = af.replace(b1, resultados["b1"])

    bf = bf.replace(a1, resultados["a1"])
    bf = bf.replace(b1, resultados["b1"])

    print("result af=", af)
    print("result bf=", bf)

    return {"A": af, "B": bf}


enes = [7, 10]

mvalues = {}
for n in enes:
    a, b, z = symbols('a b z')
    result = solve(a, b, n=n)
    p = symbols("p")
    q = 1 - p

    A = result["A"]
    B = result["B"]

    Az = A.replace(a, p*z).replace(b, q*z)

    Bz = B.replace(a, p*z).replace(b, q*z)

    print("Resultado latex A")
    print(latex(expand(Az)))

    print("Resultado latex B")
    print(latex(expand(Bz)))

    Pa = Az.replace(z, 1)
    Pb = Bz.replace(z, 1)

    Ea = diff(Az, z).replace(z, 1)
    Eb = diff(Bz, z).replace(z, 1)

    print("Resultado gana A")
    print(latex(expand(Pa)))

    print("Resultado gana B")
    print(latex(expand(Pb)))

    print("Resultado ave(A)")
    print(latex(expand(Pa)))

    print("Resultado ave(B)")
    print(latex(expand(Pb)))

    p1 = plot(Pa, (p, 0, 1), show=False, legend=True,
              label="Pa", title=f"G_{n}(p)")
    p2 = plot(Pb, (p, 0, 1), show=False, label="Pb")
    p1.append(p2[0])
    p1.save(f"./p_a_p_b_n{n}.png")
    p1.show()

    p1 = plot(Ea, (p, 0, 1), show=False, legend=True, label="Ea")
    p2 = plot(Eb, (p, 0, 1), show=False, label="Eb")
    p1.append(p2[0])
    p1.save(f"./ave_a_ave_b_n{n}.png")
    p1.show()

    ps = plot(Ea+Eb, (p, 0, 1), show=False,
              title=f"Valor esperado de juegos, n={n}")
    ps.save(f"./sum_ave_a_ave_b_n{n}.png")
    ps.show()

    Juegos = Ea + Eb
    max_value = Juegos.replace(p, 0.5)
    mvalues[n] = max_value
    #
print(f"Maximo valor", mvalues)
